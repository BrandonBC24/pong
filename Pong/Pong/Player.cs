﻿using System;

namespace Pong
{
    class Player : AnimatedObject
    {
        protected byte id;
        protected uint points { get; set; }

        public Player(byte id, short x, short y) : base(x, y)
        {
            this.id = id;
            points = 0;
        }

        public void Move(ConsoleKey key)
        {
            if (key == ConsoleKey.UpArrow && y > 1)
                y--;
            if (key == ConsoleKey.DownArrow && y < HEIGT - 2)
                y++;
        }
    }
}
