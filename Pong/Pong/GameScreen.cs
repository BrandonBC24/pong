﻿using System;

namespace Pong
{
    class GameScreen
    {
        protected int WIDTH;
        protected int HEIGT;

        public GameScreen()
        {
            WIDTH = Console.WindowWidth;
            HEIGT = Console.WindowHeight;
        }

        public void DrawMap()
        {
            string line = new string('-', WIDTH);
            Console.WriteLine(line);

            Console.WriteLine(line);
        }

        public void Scoreboard()
        {
            int scoreJ1 = 0;
            int scoreJ2 = 0;
            Console.SetCursorPosition(5, WIDTH / 2);
            Console.Write(scoreJ1);
            Console.WriteLine(scoreJ2);
        }

        public void Run()
        {
            Player player1 = new Player(0, 0, 0);
            Player player2 = new Player(1, 0, 0);
            Ball ball = new Ball();

            player1.Draw();
            player2.Draw();
            ball.Draw();
        }

    }
}
