﻿// Javier Cases, Guillermo Pastor, Brandom

namespace Pong
{
    class Program
    {
        static void Main(string[] args)
        {
            GameScreen game = new GameScreen();
            game.Run();
        }
    }
}
