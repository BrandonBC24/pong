﻿using System;

namespace Pong
{
    abstract class AnimatedObject : GameScreen
    {
        protected short x, y;

        public AnimatedObject(short x, short y)
        {
            this.x = x;
            this.y = y;
        }

        public virtual void Draw() { Console.SetCursorPosition(x, y); }
    }
}
